
/* načtení knihoven */
#include <ESP8266WiFi.h>
#include <Arduino.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Adafruit_NeoPixel.h>
#include <FS.h>
#include <Wire.h>
#include "LittleFS.h"

#define PIN D7

#define NUMPIXELS 7

const char *ssid = "XXX";
const char *password = "XXX";

/* nastavení konstant periferií */
const int led_deska = LED_BUILTIN;

char* ledState;

AsyncWebServer server(80);

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
volatile uint8_t mode = 0;
volatile uint8_t red = 0;
volatile uint8_t green = 0;
volatile uint8_t blue = 0;

void setup(){
  /* debug */
  pixels.begin();
  Serial.begin(115200);  

  /* nastavení LED_BUILTIN */
  pinMode(led_deska, OUTPUT);
  digitalWrite(led_deska, LOW);

  /* připojení LittleFS */
  if(!LittleFS.begin()){
    Serial.println("Při připojování LittleFS vznikla chyba");
    return;
  }

  /* připojení WiFi v modu STA*/
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  /* proces připojování na WiFi s finálním výpisem do konzole */
  Serial.println("");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("\nPřipojeno na: ");
  Serial.println(ssid);
  Serial.print("IP adresa: ");
  Serial.println(WiFi.localIP());
  /* spuštění Multicast DNS - přiděluji adresy ke jménům v malé síti */
  // if (MDNS.begin("esp8266")) {Serial.println("MDNS responder started");}

   server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
   request->send(LittleFS, "/index.html", String(), false);
  });

  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/style.css", "text/css");
  });
  /* /prepni - změna stavu LED */
    server.on("/1", HTTP_GET, [](AsyncWebServerRequest *request){
    mode = 1;
    request->redirect("/");
  });
  
  server.on("/2", HTTP_GET, [](AsyncWebServerRequest *request){
    mode = 2;
    request->redirect("/");
  });
  
  server.on("/3", HTTP_GET, [](AsyncWebServerRequest *request){
    mode = 3;
    request->redirect("/");
  });
  server.on("/setColor", HTTP_GET, [&pixels](AsyncWebServerRequest *request){
    mode=0;
    red = request->getParam("red")->value().toInt();
    green = request->getParam("green")->value().toInt();
    blue = request->getParam("blue")->value().toInt();   
    request->redirect("/");
  });

  server.begin();
}

void mode1(){
    for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i,pixels.Color(red,green,blue));
    pixels.show();
    delay(250);
    }
    for(int i=0; i<NUMPIXELS;i++){
       pixels.setPixelColor(i,pixels.Color(0,0,0));
        pixels.show();
    delay(250);
    }
 }
 void mode2(){
    
   int redRand = random(128);
   int greenRand = random(128);
   int blueRand = random(128);
   for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i,pixels.Color(redRand,greenRand,blueRand));
    pixels.show();
   }
   delay(250);
 }
 void mode3(){
  for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i,pixels.Color(red,green,blue));
    pixels.show();
    }
    delay(300);
    for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i,pixels.Color(0,0,0));
    pixels.show();
    }
    delay(300);
 }
 
void loop(){

  switch(mode){
    case 0 :   
      for(int i=0;i<NUMPIXELS;i++){
      pixels.setPixelColor(i,pixels.Color(red,green,blue));
      pixels.show();
      }
      break;
    case 1 :
      pixels.clear();
      mode1();
      break;
    case 2 : 
      pixels.clear();
      mode2();
      break;
    case 3 :
      pixels.clear();
      mode3();
      break;
  }
  delay(1500);
}
